# ArrayAccessTrait

Adds the basic methods for accessing properties of an object using array notation.

For example:

```
$foo = new Foo;

$foo['bar'] = 'bar';

echo $foo['bar']; // 'bar'

unset( $foo['bar'] );

var_dump( isset($foo['bar']) ); // 'false'
```

Properties created in this way are stored in a private array called 'container'.

Any class that uses this trait must implement the ArrayAccess interface, see http://php.net/manual/en/class.arrayaccess.php.

For example:

```
class Foo implements \ArrayAccess
{
}
```

## Changelog
* v2.0 PHP 8 Compatibility
* v1.0 Initial release