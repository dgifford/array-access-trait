<?php
Namespace dgifford\Traits;

/*
	Trait that implements array access using a container variable.


	Copyright (C) 2016  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



trait ArrayAccessTrait
{
    /**
     * @param mixed $index
     * @param mixed $properties
     * @return void
     */
    public function offsetSet(mixed $index, mixed $properties ): void
	{
		if( is_null($index) )
		{
			$this->container[] = $properties;
		}
		else
		{
			$this->container[ $index ] = $properties;
		}
	}


    /**
     * @param mixed $index
     * @return bool
     */
    public function offsetExists(mixed $index ): bool
	{
		return isset($this->container[ $index ]);
	}


    /**
     * @param mixed $index
     * @return void
     */
    public function offsetUnset(mixed $index ): void
	{
		if( isset($this->container[ $index ]) )
		{
			unset($this->container[ $index ]);
		}
	}


    /**
     * @param mixed $index
     * @return mixed
     */
    public function offsetGet(mixed $index ): mixed
	{
		if( isset($this->container[ $index ]) )
		{
			return $this->container[ $index ];
		}

		return null;
	}


    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->container);
    }
}