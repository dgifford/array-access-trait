<?php
Namespace dgifford\Traits\Tests;

use dgifford\Traits\ArrayAccessTrait;



/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');


class ArrayAccessTraitTest extends \PHPUnit\Framework\TestCase
{
    public $mock;


	public function setUp(): void
	{
		$this->mock = new Mock;

		$this->mock['test'] = 'wibble';
	}



	public function testAccessProperty()
	{
		$this->assertSame( 'wibble', $this->mock['test'] );
	}



	public function testUnsetProperty()
	{
		unset($this->mock['test']);
		
		$this->assertSame( null, $this->mock['test'] );
	}



	public function testIsPropertySet()
	{
		$this->assertSame( true, isset($this->mock['test']) );
	}



	public function testCount()
	{
		$this->assertSame( 1, $this->mock->count() );
	}



    public function testAppendValue()
    {
        $this->assertSame( 1, $this->mock->count() );

        $this->mock[] = 'foo';

        $this->assertSame( 2, $this->mock->count() );
    }
}