<?php

namespace dgifford\Traits\Tests;

use dgifford\Traits\ArrayAccessTrait;

class Mock implements \ArrayAccess
{
    use ArrayAccessTrait;

    public array $container;
}